package com.victormazali.register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.victormazali.register.DAO.DAO;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.victormazali.register.MESSAGE";
    public Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //chamar sessão
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = preferences.edit();

        String section = preferences.getString("Name","");

        if ( !section.isEmpty() ){
                      Log.e("DEBUG/SESSAO", "Sessao ja existe! (" + section + ")" );
            startActivity( new Intent(MainActivity.this, LoggedActivity.class) );

        } else {
            Log.e("DEBUG/SESSAO", "Sessao nao iniciada!" );
        }

        setContentView(R.layout.activity_main);
        Button acessar = (Button)findViewById(R.id.logmain);
        Button registrar = (Button)findViewById(R.id.regmain);
        final EditText email = (EditText) findViewById(R.id.emaillog);
        final EditText senha = (EditText) findViewById(R.id.passlog);
        acessar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(MainActivity.this, ListActivity.class));
                Bundle mBundle = new Bundle();

                DAO dao = new DAO( getApplicationContext() );

                Cursor c = dao.select("*","USUARIO","EMAIL = '" + email.getText().toString() +"' AND SENHA = '" + senha.getText().toString() + "'");
                String email = "";
                String nomeUsuario = "";
                if (c.getCount() > 0) {
                    c.moveToFirst();
                    email = c.getString(2);
                    nomeUsuario = c.getString(1);
                    editor.putString("Name",nomeUsuario);
                    editor.apply();
                    startActivity(new Intent(MainActivity.this, LoggedActivity.class));

                } else{
                    startActivity(new Intent(MainActivity.this, RegisterActivity.class));
                }
            }
        });
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
            }
        });

    }
}
