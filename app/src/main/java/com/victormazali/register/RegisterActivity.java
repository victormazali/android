package com.victormazali.register;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.victormazali.register.DAO.DAO;

public class RegisterActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //chamar sessão
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = preferences.edit();

        String name = preferences.getString("Name", "");


        Log.d("DEBUG", "Nome sessao" + name);

        Button acessar = (Button)findViewById(R.id.subreg);
        Button voltar = (Button)findViewById(R.id.backreg);
        final EditText inputNome = (EditText) findViewById( R.id.namereg );
        final EditText inputEmail = (EditText) findViewById( R.id.emailreg );
        final EditText inputSenha = (EditText) findViewById( R.id.passreg );

        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, MainActivity.class));
            }
        });

        acessar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ContentValues valores;
                long resultado;

                DAO dao = new DAO( getApplicationContext() );

                valores = new ContentValues();
                valores.put("NOME", inputNome.getText().toString() );
                valores.put("EMAIL", inputEmail.getText().toString() );
                valores.put("SENHA", inputSenha.getText().toString() );

                resultado = dao.inserir("USUARIO",valores);

                editor.putString("Name",inputNome.getText().toString() );
                editor.apply();

                startActivity(new Intent(RegisterActivity.this, LoggedActivity.class));
            }
        });

    }

}
