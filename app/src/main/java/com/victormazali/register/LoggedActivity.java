package com.victormazali.register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoggedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = preferences.edit();
        Intent intent = getIntent();
        TextView nameview = (TextView) findViewById(R.id.namelogged);
        String name = preferences.getString("Name", "");
        nameview.setText(name);


        Button sair = (Button)findViewById(R.id.logout);
        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("Name","");
                editor.apply();
                startActivity(new Intent(LoggedActivity.this, MainActivity.class));
            }
        });
    }

}
